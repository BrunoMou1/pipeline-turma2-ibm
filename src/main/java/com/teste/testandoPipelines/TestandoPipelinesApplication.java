package com.teste.testandoPipelines;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestandoPipelinesApplication {

	public static void main(String[] args) throws IOException {
		System.out.println("Digite seu cpf");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String cpf = reader.readLine();

		System.out.println("O CPF digitado é: " + 
			(ValidadorCPF.validar(cpf) ? "valido" : "invalido")
		);
		//SpringApplication.run(TestandoPipelinesApplication.class, args);
	}

}
