package com.teste.testandoPipelines;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TestandoPipelinesApplicationTests {

	@Test
	void validarCPFTest() {
		boolean cpf = ValidadorCPF.validar("70338606416");
		assertEquals(true, cpf);
	}

	@Test
	void invalidarCPFTest() {
		boolean cpf = ValidadorCPF.validar("44444444444");
		assertEquals(false, cpf);
	}
}
